@extends('inc.layout')

@section('content')
    <div class="row">
        <div class="col-md-8 col pb-5">
            <h1>{{ the_title() }}</h1>
            {!! the_image('mw-100 mt-2 mb-2') !!}
            {!! the_content() !!}
        </div>
        <div class="col-md-4 col pl-5 pr-5 pb-5">
            @include('inc.sidebar')
        </div>
    </div>
@endsection
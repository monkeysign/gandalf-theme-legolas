@extends('inc.layout')

@section('content')
    <div class="row">
        <div class="col-md-8 col pb-5">
            @if(count($posts) == 0)<h6 class="text-secondary">Nessun record presente...</h6>@endif
            @foreach($posts as $single)
                <div class="row">
                    <div class="col-12">
                        <h3><a href="{{path_for('page', ['permalink' => $single->permalink])}}">{{$single->meta('title')}}</a></h3>
                        <p>{!! $single->meta('excerpt') !!}</p>
                    </div>
                </div>
                <hr />
            @endforeach
        </div>
        <div class="col-md-4 col pl-5 pr-5 pb-5">
            @include('inc.sidebar')
        </div>
    </div>
@endsection
@extends('inc.layout')

@section('content')
    <div class="row">
        <div class="col">
            {!! the_content() !!}
        </div>
    </div>
@endsection
<?php
/**
 * Ritorna l'url di un asset
 */
function theme() {
	return config('frontend_theme');
}

/**
 * general api
 */
require __DIR__ . '/../../api.php';

/**
 * Load function theme
 */
require __DIR__ .'/../functions.php';